import json
import random

with open ('bank_accounts.json') as f:
    val = json.load(f)

class Mybank:

    @staticmethod
    def Check_Balance(single_data):
        total_amt = single_data['total_amount']
        password = single_data['pin']

        password_input = int(input("Enter your pin here: "))
        if password_input == password:
            print(f"You have a balance of Rs. {total_amt} in your Bank Account !")
        else:
            raise ValueError("Password is incorrect..! Try Again..!")
    
    @staticmethod
    def val_of_withdrawal(single_data):
        name = single_data['account_name']
        acc_no = single_data['account_no']
        total_amt = single_data['total_amount']
        password = single_data['pin']
        
        withdraw = int(input("Enter Amount you want to Withdraw: "))
        password_input = int(input("Enter your pin here: "))
        if password_input == password:   

            if withdraw > total_amt:
                print("Your Withdrawal amount is greater than your Bank Balance : ")
                print(name, acc_no, total_amt)
            else:
                Remaining_amt = total_amt - withdraw
                print(f"Withdrawal of Rs. {withdraw} Succesfully..! Now you have Balance of Rs. {Remaining_amt} in your Bank Account..!") 

        else:
            raise ValueError("Password is incorrect..! Try Again..!")

        return Remaining_amt    
           
    @staticmethod
    def unlimited_deposite(single_data):
        total_amt = single_data['total_amount']
        password = single_data['pin']
        
        Deposite = int(input("Enter Amount you want to Deposite in your Bank Account : "))
        password_input = int(input("Enter your pin here: "))
        if password_input == password:                  
            Remaining_amt = total_amt + Deposite
            print(f"Deposite of Rs. {Deposite} Succesfully..! Now you have Balance of Rs. {Remaining_amt} in your Bank Account..!") 
        else:
            raise ValueError("Password is incorrect..! Try Again..!")

        return Remaining_amt                 

    @staticmethod
    def addcontact():
        my_dict = {}
        bank = "HDFC"
        Ifsc = 96695

        Acc_number = int(random.randint(10000000000, 99999999999))
        if len(str(Acc_number)) < 11 or len(str(Acc_number)) > 11 :
            raise ValueError("Account no. of HDFC Bank's user should be of 11 digit !")    # validation of Account Number

        name = input("Enter your Name: ")

        email = input("Enter Email here: ")
        if "@gmail.com" not in email:
            raise ValueError("Invalid Email Address..! Email address must contain @gmail.com ")     # Validation of Email Address
        
        phone = int(input("Enter your Mobile No.: "))
        if len(str(phone)) < 10 or len(str(phone)) > 10:
            raise ValueError("Invalid Phone Number..! Phone No. should be of 10 digit ")   # validation of Phone No
        
        address = input("Enter your Address: ")
        
        pin = int(input("Enter 6 digit pin: "))
        if len(str(pin)) < 6 or len(str(pin)) > 6:
            raise ValueError("pin must be of 6 digit")    # Validation of pin
        
        print("You must Deposite some Amount to Activate your Bank Account, Please Deposite atleast Rs. 1000 in your Bank Account..!")
        Deposite = int(input("Enter amount you want to Deposite: "))
        if Deposite < 1000:
            raise Exception("You need to Deposite at least 1000 Rs...!")
        
        Total = Deposite
        acc_type = input("Enter your account type: ")

        is_act = True

        my_dict.update({'account_no': Acc_number, 'account_name': name, 'bank_name': bank, 'email': email, 'phone_no': phone, 
                     'address': address, 'pin': pin, 'total_amount': Total,
                     'IFSC_CODE': Ifsc, 'is_active': is_act, 'account_type': acc_type})
        
        return my_dict


obj = Mybank()

user_input = input("Do you have an Account in HDFC Bank ? Enter Y for Yes & N for No: ") 
if user_input == "N" or user_input == "n" :
    ask_user = input("Do you want to create your Account? Enter Y for Yes & N for No: ")
    if ask_user == "Y" or ask_user == "y":
        my_dict = obj.addcontact()

        val['data'].append(my_dict)

        with open ('bank_accounts.json', 'w') as file:
            json.dump(val, file, indent=4)
            print("Account created Succesfully...Thank You..!")

    elif ask_user == "N" or ask_user == "n":
        exit

    else:
        print("Please enter correct Option..!")
        exit

elif user_input == "Y" or user_input == "y":
    user_input_2 = input("Enter your Name: ")
    user_input_3 = input("Enter your Account No.: ") 
    for single_data in val['data']: 
        name = single_data['account_name']
        acc_no = single_data["account_no"]  

        if user_input_2  == name and user_input_3 == acc_no:

            print("You are Succesfully Entered..")
            print("Enter the number before option as per your requirement \n 1. Check Bank Balance \n 2. Withdraw \n 3. Deposite \n 4. Exit ")

            while True:
                choice = int(input("Enetr your Choice: "))
                if choice == 1:
                    obj.Check_Balance(single_data)

                elif choice == 2:
                    Remaining_amt = obj.val_of_withdrawal(single_data)

                    single_data["total_amount"] = Remaining_amt
                    
                elif choice == 3:
                    Remaining_amt  = obj.unlimited_deposite(single_data)

                    single_data["total_amount"] = Remaining_amt

                elif choice == 4:
                    print("Thank you..!")
                    break

                else:
                    print("Please enter currect option..!")

    with open ('bank_accounts.json', 'w') as file:
        json.dump(val, file, indent=4)             

else:
    print("You entered Invalid input..!")            
    exit